# Feed Reader

Simple RSS feed reader built with Laravel, React, and Redux.

View an example list of feeds at [https://tdm.bz/rss/#view/news](https://tdm.bz/rss/#view/news) or create your own at [https://tdm.bz/rss](https://tdm.bz/rss).
