@extends("layout")

@section("title")
    <title>Feeds</title>
@stop

@section("content")
    <div id="root"></div>
@stop

@section("scripts")
    <script src="{{ URL::asset('js/show.js') }}"></script>
@stop
