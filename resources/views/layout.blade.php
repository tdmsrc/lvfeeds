<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name=viewport content="width=device-width,initial-scale=1">
        
        @yield("title")
        
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        
        <link rel=stylesheet href="{{ URL::asset('css/main.css') }}" type="text/css">
    </head>
    
    <body>
        
        @yield("content")
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/es5-shim/4.5.9/es5-shim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.1/es6-shim.min.js"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
        
        <script>
            window._EXPORTS = {
                "csrftoken": "{!! csrf_token() !!}"
            };
            
            window._ROUTES = {                
                "login": function(){ return "{!! route("login") !!}"; },
                "logout": function(){ return "{!! route("logout") !!}"; },
                
                "suggested": function(){ return "{!! route("suggested") !!}"; },
                "get": function(){ return "{!! route("get") !!}"; },
                "add": function(){ return "{!! route("add") !!}"; },
                "remove": function(){ return "{!! route("remove") !!}"; },
                "adduser": function(){ return "{!! route("adduser") !!}"; },
                
                "feedapi": function(){ return "{!! route("feedapi") !!}"; },
                
                "clientroot": function(){ return "{!! route("show") !!}"; }
            };
        </script>
        
        @yield("scripts")
        
    </body>
</html>
