
import React from "react";
import ReactDOM from "react-dom";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import ReduxThunk from "redux-thunk";

import { AuthReducer } from "./store/auth";
import { AjaxReducer } from "./store/ajax";
import { LayoutActions, LayoutReducer } from "./store/layout";
import { FeedsActions, FeedsReducer } from "./store/feeds";

import { default as Layout } from "./components/layout";


//root reducer and store
//==================================

//root reducer
var RootReducer = combineReducers({
    auth:   AuthReducer,
    layout: LayoutReducer,
    ajax:   AjaxReducer,
    feeds:  FeedsReducer
});

//store and middleware
var logger = store => next => action => {
    console.log('dispatching', action);
    let result = next(action);
    console.log(store.getState());
    return result;
};

var store = createStore(
    RootReducer,
    //applyMiddleware(logger, ReduxThunk)
    applyMiddleware(ReduxThunk)
);

//hash routing: location is array of values with delimiter '/'
var onHashChange = function(){
    var location = window.location.hash.replace(/^#\/?|\/$/g, '').split('/');
    store.dispatch(LayoutActions.setLocation(location));
};

onHashChange();
window.addEventListener('hashchange', onHashChange, false);

//initialize
store.dispatch(FeedsActions.fetchSuggested());

ReactDOM.render(
    <Provider store={store}>
        <Layout />
    </Provider>,
    document.getElementById("root")
);



