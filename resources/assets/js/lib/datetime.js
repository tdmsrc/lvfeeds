
var monthStringShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

var DateTimeHelper = {
    
    nextHourTimestamp: function(ts){
        var date = new Date(parseInt(ts)*1000);
        return ts - date.getSeconds() + 60*(60 - date.getMinutes());
    },
    
    sameDay: function(date1, date2){
        return (
            (date1.getFullYear() == date2.getFullYear()) &&
            (date1.getMonth() == date2.getMonth()) &&
            (date1.getDate() == date2.getDate())
        );
    },
    
    prettyTimestamp: function(ts){
        var date = new Date(parseInt(ts)*1000);
        
        //date
        var mStr = monthStringShort[date.getMonth()] + " ";
        var dStr = String(date.getDate());
        var dateString = "on " + mStr + dStr;
        
        var dateToday = new Date();
        
        var dateYesterday = new Date();
        dateYesterday.setHours(dateYesterday.getHours() - 24);
        var dateTomorrow = new Date();
        dateTomorrow.setHours(dateTomorrow.getHours() + 24);
        
        if(DateTimeHelper.sameDay(date, dateToday)){
            dateString = "Today";
        }else if(DateTimeHelper.sameDay(date, dateYesterday)){
            dateString = "Yesterday";
        }else if(DateTimeHelper.sameDay(date, dateTomorrow)){
            dateString = "Tomorrow";
        }
        
        //time
        var padMin = String(date.getMinutes());
        if(padMin.length == 1){ padMin = "0" + padMin; }
        
        var h24 = date.getHours();
        var ampm = (h24 < 12) ? " AM" : " PM";
        
        var h12 = h24 % 12;
        if(h12 == 0){ h12 = 12; }
        var padH = String(h12);
        
        var timeString = padH + ":" + padMin + ampm;
        if(date.getMinutes() == 0){
            if(h24 == 0){ timeString = "Midnight"; }
            if(h24 == 12){ timeString = "Noon"; }
        }
        
        //combine
        return timeString + " " + dateString;
    }
};
    
    
export default DateTimeHelper;

