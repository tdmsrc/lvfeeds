
import { AjaxActions } from "./ajax";


var initialState = {
    valid: false,
    urlkey: ""
};

//reducer
var AuthReducer = (state = initialState, action) => {
    switch(action.type){
    case "AUTH_LOGIN":
        return Object.assign({}, state, { valid: true, urlkey: action.urlkey });
    case "AUTH_LOGOUT":
        return Object.assign({}, state, { valid: false, urlkey: "" });
    default:
        return state;
    }
};

//action creators
var AuthActions = { };

var _AuthActions = {
    login: (urlkey) => ({ 
        type: "AUTH_LOGIN",
        urlkey: urlkey
    }),
    
    logout: (urlkey) => ({ type: "AUTH_LOGOUT" })
};

//thunk action creators
AuthActions.login = (urlkey, password, onSuccess, onFail) => (dispatch) => {

    var _onSuccess = (dispatch, data) => {
        onSuccess(dispatch, data);
        dispatch(_AuthActions.login(urlkey));
    };
    var _onFail = (dispatch, errmsg) => {
        onFail(dispatch, errmsg);
        dispatch(_AuthActions.logout());
    };
    
    dispatch(AjaxActions.post(_ROUTES["login"](), {urlkey: urlkey, password: password}, _onSuccess, _onFail));
};

AuthActions.logout = () => (dispatch) => {
    var onSuccess = (dispatch, data) => { };
    
    dispatch(AjaxActions.post(_ROUTES["logout"](), {}, onSuccess));
    dispatch(_AuthActions.logout());
};


export { AuthReducer, AuthActions };

