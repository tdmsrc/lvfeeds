
var initialState = {
    pending: 0
};

//reducer
var AjaxReducer = (state = initialState, action) => {
    switch(action.type){
    case "AJAX_INCR":
        return Object.assign({}, state, { pending: state.pending+1 });
        
    case "AJAX_DECR":
        return Object.assign({}, state, { pending: state.pending-1 });
    
    default:
        return state;
    }
};

//action creators
var AjaxActions = { };

var _AjaxActions = {
    incr: () => ({ type: "AJAX_INCR" }),
    decr: () => ({ type: "AJAX_DECR" })
};

//thunk action creator for handling standard responses, maintaining request count, etc
//assumes server will return JSON with shape { success: bool, data: [data or err msg] }
//  onSuccess has signature (dispatch, data)
//  (optional) onFail has signature (dispatch, errmsg)
_AjaxActions.request = (method, target, dataSend, onSuccess, onFail) => (dispatch) => {
    dispatch(_AjaxActions.incr());
    
    $.ajax({
        method: method, 
        url: target, 
        data: dataSend, 
        dataType: "json",
        beforeSend: function(xhr, settings){
            xhr.setRequestHeader("X-CSRF-TOKEN", _EXPORTS["csrftoken"]);
        }
    })
    
    .done((data) => {
        if(!data.success){
            var errmsg = data.data;
            if(typeof(onFail) === "function"){ onFail(dispatch, errmsg); }
        }else{
            onSuccess(dispatch, data.data);
        }
    })
    
    .fail((xhr, status, error) => {
        var errmsg = "Unable to connect";  
        if(typeof(onFail) === "function"){ onFail(dispatch, errmsg); }
    })
    
    .always(() => {
        dispatch(_AjaxActions.decr());
    });
};

AjaxActions.get = (target, dataSend, onSuccess, onFail) => 
    _AjaxActions.request("get", target, dataSend, onSuccess, onFail);

AjaxActions.post = (target, dataSend, onSuccess, onFail) => 
    _AjaxActions.request("post", target, dataSend, onSuccess, onFail);

export { AjaxReducer, AjaxActions };

