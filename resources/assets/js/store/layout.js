
import { FeedsActions } from "./feeds";


var initialState = {
    tab: "home",
    urlkey: ""
};

//reducer
var LayoutReducer = (state = initialState, action) => {
    switch(action.type){
    case "LAYOUT_SET_TAB":
        return Object.assign({}, state, { tab: action.tab });
    case "LAYOUT_SET_URLKEY":
        return Object.assign({}, state, { urlkey: action.urlkey });
    default:
        return state;
    }
};

//action creators
var LayoutActions = { };

var _LayoutActions = {
    setTab: (tab) => ({
        type: "LAYOUT_SET_TAB",
        tab: tab
    }),
    
    setURLKey: (urlkey) => ({
        type: "LAYOUT_SET_URLKEY",
        urlkey: urlkey
    })
};

//thunk action creators

//NOTE: only syncs if urlkey has changed
_LayoutActions.setURLKeyAndSync = (urlkey, onFail) => (dispatch, getState) => {
    var state = getState();
    var urlkeyOld = state.layout.urlkey;
    
    if(urlkey != urlkeyOld){
        dispatch(_LayoutActions.setURLKey(urlkey));
        dispatch(FeedsActions.localSync(urlkey, onFail));
    }
};

LayoutActions.setLocation = (location) => (dispatch) => {
    var page = location[0];
    
    var onFail = (dispatch, errmsg) => {
        console.warn(errmsg); //TODO
    };
    
    switch(page){
    case "home": 
        dispatch(_LayoutActions.setTab("home"));
        document.title = "Feeds - Home";
        return;
        
    case "view":
        var urlkey = location[1];
        dispatch(_LayoutActions.setURLKeyAndSync(urlkey, onFail));
        
        dispatch(_LayoutActions.setTab("view"));
        document.title = "Feeds - Viewing " + urlkey;
        return;
        
    case "edit":
        var urlkey = location[1];
        dispatch(_LayoutActions.setURLKeyAndSync(urlkey, onFail));
        
        dispatch(_LayoutActions.setTab("edit"));
        document.title = "Feeds - Editing " + urlkey;
        return;
        
    default:
        onFail(dispatch, "Unknown location");
        window.location.hash = "#home";
    }
};


export { LayoutReducer, LayoutActions };

