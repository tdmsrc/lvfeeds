
import { AjaxActions } from "./ajax";


//suggested = array of urls
//feeds = array of (id, {status, data}) KV pairs
//  status = "done" (data is valid) || "pending" (ajax in progress) || "broken" (fetching failed)
//  data = {feedData, items} (i.e., as returned from API)
//    feedData = {url, link, title, description, icon (url)}
//    items = array of {link, title, description, timestamp}

var initialState = {
    feeds: {},
    suggested: []
};

//helper to give valid data for empty feeds
var newEmptyData = (url) => ({
    feedData: {
        url: url,
        link: "",
        title: url,
        description: "",
        icon: ""
    },
    items: []
});

//reducer
var FeedsReducer = (state = initialState, action) => {
    switch(action.type){
    case "FEEDS_SET_SUGGESTED":
        return Object.assign({}, state, { suggested: action.suggested });
    
    case "FEEDS_LOCAL_CLEAR":
        var newFeeds = {};
        for(var i=0; i<action.keep.length; i++){
            var id = action.keep[i];
            if(state.feeds[id] === undefined){ continue; }
            newFeeds[id] = state.feeds[id];
        }
        
        return Object.assign({}, state, { feeds: newFeeds });
    
    case "FEEDS_LOCAL_REMOVE":
        var newFeeds = {};
        for(var id in state.feeds){
            if(id == action.id){ continue; }
            newFeeds[id] = state.feeds[id];
        }
        
        return Object.assign({}, state, { feeds: newFeeds });
    
    case "FEEDS_LOCAL_SET_STATUS":
        var newData = (state.feeds[action.id] === undefined) ?
            newEmptyData(action.url) :
            Object.assign({}, state.feeds[action.id].data);
        
        var newFeeds = Object.assign({}, state.feeds);
        newFeeds[action.id] = { 
            status: action.status,
            data: newData
        };
        
        return Object.assign({}, state, { feeds: newFeeds });
        
    case "FEEDS_LOCAL_SET_DATA":
        var newFeeds = Object.assign({}, state.feeds);
        newFeeds[action.id] = { status: "done", data: action.data };
        
        return Object.assign({}, state, { feeds: newFeeds });
    
    default: 
        return state;
    }
};

//action creators
var FeedsActions = { };

var _FeedsActions = {
    setSuggested: (suggested) => ({
        type: "FEEDS_SET_SUGGESTED",
        suggested: suggested
    }),
    
    //remove all feeds except those with id present in "keep" array
    localClear: (keep) => ({ 
        type: "FEEDS_LOCAL_CLEAR",
        keep: keep
    }),
    
    localRemove: (id) => ({ 
        type: "FEEDS_LOCAL_REMOVE",
        id: id
    }),
    
    //add id if necessary, set status as given, leave data alone if exists
    localSetStatus: (id, url, status) => ({
        type: "FEEDS_LOCAL_SET_STATUS",
        id: id,
        url: url,
        status: status
    }),
    
    //add id if necessary, set status = "done", set given data
    localSetData: (id, data) => ({
        type: "FEEDS_LOCAL_SET_DATA",
        id: id,
        data: data
    })
};

//thunk action creators
FeedsActions.fetchSuggested = () => (dispatch) => {
    
    var onSuccess = (dispatch, data) => {
        dispatch(_FeedsActions.setSuggested(data));
    };
    dispatch(AjaxActions.post(_ROUTES["suggested"](), {}, onSuccess));
};

//items = array of {id, url}
_FeedsActions.localAddAndFetch = (items) => (dispatch) => {
    
    var onSuccess = (item) => (dispatch, data) => {
        dispatch(_FeedsActions.localSetData(item.id, data));
    };
    var onFail = (item) => (dispatch, errmsg) => {
        dispatch(_FeedsActions.localSetStatus(item.id, item.url, "broken"));
    };
    
    for(var i=0; i<items.length; i++){
        var item = items[i];
        dispatch(_FeedsActions.localSetStatus(item.id, item.url, "pending"));
        dispatch(AjaxActions.get(_ROUTES["feedapi"](), {url: item.url}, onSuccess(item), onFail(item)));
    }
};

FeedsActions.localSync = (urlkey, onFail) => (dispatch) => {
    
    //data = array of {id, url}
    var onSuccess = (dispatch, data) => {
        var keep = data.map((item) => item.id);
        dispatch(_FeedsActions.localClear(keep));
        dispatch(_FeedsActions.localAddAndFetch(data));
    };
    var _onFail = (dispatch, errmsg) => {
        var keep = [];
        dispatch(_FeedsActions.localClear(keep));
        onFail(dispatch, errmsg);
    };
    dispatch(AjaxActions.post(_ROUTES["get"](), {urlkey: urlkey}, onSuccess, _onFail));
};

FeedsActions.remoteAdd = (urlkey, url, onSuccess, onFail) => (dispatch) => {
    
    //data = {id, url}
    var _onSuccess = (dispatch, data) => {
        onSuccess(dispatch, data);
        dispatch(_FeedsActions.localAddAndFetch([data]));
    };
    dispatch(AjaxActions.post(_ROUTES["add"](), {urlkey: urlkey, feedurl: url}, _onSuccess, onFail));
};

FeedsActions.remoteRemove = (urlkey, id, onSuccess, onFail) => (dispatch) => {
    
    //data = empty
    var _onSuccess = (dispatch, data) => {
        onSuccess(dispatch, data);
        dispatch(_FeedsActions.localRemove(id));
    };
    dispatch(AjaxActions.post(_ROUTES["remove"](), {urlkey: urlkey, feedid: id}, _onSuccess, onFail));
};


export { FeedsReducer, FeedsActions };

