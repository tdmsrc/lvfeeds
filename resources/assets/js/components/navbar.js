
import React from "react";
import { connect } from "react-redux";

import { LayoutActions } from "../store/layout";
import { FeedsActions } from "../store/feeds";


var Navbar = React.createClass({
    
    onClickRefresh: function(){
        var onFail = (dispatch, errmsg) => { };
        this.props.dispatch(FeedsActions.localSync(this.props.urlkey, onFail));
    },
    
    //TODO duplicate in HomePage
    renderBrand: function(){
        
        var pendingCount = ""; //(this.props.pendingAJAX > 0) ? this.props.pendingAJAX : "";
        
        var brandIcon = (this.props.pendingAJAX > 0) ? 
            (<span className="glyphicon glyphicon-spin glyphicon-refresh"></span>) :
            (<span className="glyphicon glyphicon-file"></span>);
        
        return (
            <span className="navbar-brand">
                {brandIcon} {pendingCount} <a href="#home">Feeds</a>
            </span>
        )
    },
    
    render: function(){
        return (
            <nav className="navbar navbar-default navbar-fixed-top">
            <div className="container">
                
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    {this.renderBrand()}
                </div>
                
                <div id="navbar" className="collapse navbar-collapse">
                    <ul className="nav navbar-nav">
                    <li className={(this.props.tab == "view") ? "active" : ""}>
                        <a href={"#view/" + this.props.urlkey}>
                            <span className="glyphicon glyphicon-list"></span> View <span className="badge">{this.props.itemCount}</span>
                        </a>
                    </li>
                    <li className={(this.props.tab == "edit") ? "active" : ""}>
                        <a href={"#edit/" + this.props.urlkey}>
                            <span className="glyphicon glyphicon-wrench"></span> Edit
                        </a>
                    </li>
                    <li>
                        <a onClick={this.onClickRefresh}>
                            <span className="glyphicon glyphicon-refresh"></span> Refresh
                        </a>
                    </li>
                    </ul>
                </div>
                
            </div>
            </nav>
        );
    }
});


var derivedItemCount = (feeds) => {
    var n = 0;
    for(var id in feeds){
        var feed = feeds[id];
        n += feed.data.items.length;
    }
    return n;
};

var derivedFeedIDs = (feeds) => {
    var feedids = [];
    for(var id in feeds){
        feedids.push({
            id: id,
            url: feeds[id].data.feedData.url
        });
    }
    return feedids;
};

//additional props: urlkey, tab
var mapStateToProps = (state) => ({
    pendingAJAX:    state.ajax.pending,
    itemCount:      derivedItemCount(state.feeds.feeds),
    feedids:        derivedFeedIDs(state.feeds.feeds)
});

export default connect(mapStateToProps)(Navbar);

