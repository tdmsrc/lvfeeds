
import React from "react";
import { connect } from "react-redux";

import { AuthActions } from "../store/auth";


var LoginForm = React.createClass({
    
    getInitialState: function(){
        return { 
            formError: "",
            formErrorExists: false,
            password: ""
        };
    },
    
    onChangePassword: function(e){
        this.setState({ password: e.target.value });
    },
    
    onSubmitLogin: function(e){
        
        var onSuccess = (dispatch, data) => { 
            this.setState({ formError: "", formErrorExists: false }); 
        };
        var onFail = (dispatch, errmsg) => { 
            this.setState({ formError: errmsg, formErrorExists: true }); 
        };
        
        this.props.dispatch(AuthActions.login(this.props.urlkey, this.state.password, onSuccess, onFail));
        
        e.preventDefault();
    },
    
    renderFormError: function(){
        if(!this.state.formErrorExists){ return false; }
        
        return (            
            <span className="label label-danger pull-right" onClick={this.onClickDismissFormError}>
                <span className="glyphicon glyphicon-exclamation-sign"></span>
                &nbsp;
                {this.state.formError}
            </span>
        );
    },
    onClickDismissFormError: function(){
        this.setState({ formError: "", formErrorExists: false });        
    },
    
    onClickLogout: function(){
        this.props.dispatch(AuthActions.logout());
    },
    
    renderLoginForm: function(){
        return (
            <div className="panel panel-warning">
                <div className="panel-heading">
                    Enter password to edit
                    {this.renderFormError()}
                </div>
                
                <div className="panel-body">
                <form onSubmit={this.onSubmitLogin}>
                    
                    <div className="row">
                    
                        <div className="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <label htmlFor="urlkey">URL suffix</label>
                            <input type="text" id="urlkey" readOnly className="form-control" value={this.props.urlkey} />
                        </div>
                        
                        <div className="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            
                            <label htmlFor="password">Password</label>                   
                            
                            <div className="input-group">
                                <input type="password" id="password" className="form-control" value={this.state.password} onChange={this.onChangePassword} />
                                <span className="input-group-btn">
                                    <button type="submit" className="btn btn-primary">
                                        <span className="glyphicon glyphicon-log-in"></span> Log in
                                    </button>
                                </span>
                            </div>
                            
                        </div>
                    </div>
                    
                </form>
                </div>
            </div>
        );
    },
    
    renderAuthNotification: function(){
        return (
            <div className="alert alert-success">
                <button type="button" className="btn btn-primary" onClick={this.onClickLogout}>
                    <span className="glyphicon glyphicon-log-out"></span> Log out
                </button>
                &nbsp;
                You are authenticated and can edit this list!
            </div>
        );
    },
    
    render: function(){
        if(this.props.authenticated){
            return this.renderAuthNotification();
        }else{
            return this.renderLoginForm();
        }
    }
});

//additional prop: urlkey
var mapStateToProps = (state, ownProps) => ({
    authenticated: (state.auth.valid && (state.auth.urlkey == ownProps.urlkey))
});

export default connect(mapStateToProps)(LoginForm);

