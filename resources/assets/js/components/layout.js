
import React from "react";
import { connect } from "react-redux";

import { default as HomePage } from "./homepage";
import { default as Navbar } from "./navbar";
import { default as ViewFeeds } from "./viewfeeds";
import { default as EditPanel } from "./editpanel";


var Layout = React.createClass({
    
    renderHome: function(){
        return (
            <HomePage />
        );
    },
    
    renderView: function(){
        return (
            <div>
                <Navbar urlkey={this.props.urlkey} tab={this.props.tab} />
                <div className="container">
                    <ViewFeeds />
                </div>
            </div>
        );
    },
    
    renderEdit: function(){
        return (
            <div>
                <Navbar urlkey={this.props.urlkey} tab={this.props.tab} />
                <div className="container">
                    <EditPanel urlkey={this.props.urlkey} />
                </div>
            </div>
        );
    },
    
    render: function(){
        switch(this.props.tab){
        case "home": 
            return this.renderHome();
        case "view":
            return this.renderView();
        case "edit":
            return this.renderEdit();
        default:
            return (<span>Unknown tab</span>);
        }
    }
});


var mapStateToProps = (state) => ({
    urlkey: state.layout.urlkey,
    tab: state.layout.tab
});

export default connect(mapStateToProps)(Layout);

