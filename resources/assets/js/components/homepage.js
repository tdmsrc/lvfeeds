
import React from "react";
import { connect } from "react-redux";

import { AjaxActions } from "../store/ajax";
import { AuthActions } from "../store/auth";
import { LayoutActions } from "../store/layout";


var HomePage = React.createClass({
    
    createUser: function(urlkey, password, onSuccess, onFail){
        
        var onSuccessAddUser = (dispatch, data) => {
            onSuccess(dispatch, data);
            
            var onSuccessLogin = (dispatch, data) => { 
                window.location.hash = "#edit/" + urlkey;
            };
            this.props.dispatch(AuthActions.login(urlkey, password, onSuccessLogin, onFail));
        };
        this.props.dispatch(AjaxActions.post(_ROUTES["adduser"](), {urlkey: urlkey, password: password}, onSuccessAddUser, onFail));
    },
    
    //TODO duplicate in Navbar
    renderBrand: function(){
        
        var pendingCount = ""; //(this.props.pendingAJAX > 0) ? this.props.pendingAJAX : "";
        
        var brandIcon = (this.props.pendingAJAX > 0) ? 
            (<span className="glyphicon glyphicon-spin glyphicon-refresh"></span>) :
            (<span className="glyphicon glyphicon-file"></span>);
        
        return (
            <span className="navbar-brand">
                {brandIcon} {pendingCount} <a href="#home">Feeds</a>
            </span>
        )
    },
    
    render: function(){
        return (
            <div>
            
            <nav className="navbar navbar-default navbar-fixed-top">
            <div className="container">
                
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    {this.renderBrand()}
                </div>
                
                <div id="navbar" className="collapse navbar-collapse">
                    <ul className="nav navbar-nav">
                    <li>
                        <a href="#view/news"><span className="glyphicon glyphicon-list"></span> Example</a>
                    </li>
                    </ul>
                </div>
                
            </div>
            </nav>
            
            <div className="container">
                <Instructions />
                <FormCreateList createUser={this.createUser} />
            </div>
            
            </div>
        );
    }
});

var FormCreateList = React.createClass({
    getInitialState: function(){
        return { 
            formError: "",
            formErrorExists: false,
            urlkey: "",
            password: ""
        };
    },
    
    onChangeURLKey: function(e){
        this.setState({ urlkey: e.target.value });
    },
    
    onChangePassword: function(e){
        this.setState({ password: e.target.value });
    },
    
    onSubmitCreate: function(e){
        
        var onSuccess = (dispatch, data) => { 
            this.setState({ formError: "", formErrorExists: false }); 
        };
        var onFail = (dispatch, errmsg) => { 
            this.setState({ formError: errmsg, formErrorExists: true }); 
        };
        
        this.props.createUser(this.state.urlkey, this.state.password, onSuccess, onFail);
        
        e.preventDefault();
    },
    
    renderFormError: function(){
        if(!this.state.formErrorExists){ return false; }
        
        return (
            <span className="label label-danger pull-right" onClick={this.onClickDismissFormError}>
                <span className="glyphicon glyphicon-exclamation-sign"></span>
                &nbsp;
                {this.state.formError}
            </span>
        );
    },
    onClickDismissFormError: function(){
        this.setState({ formError: "", formErrorExists: false });        
    },
    
    render: function(){
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    Create List
                    {this.renderFormError()}
                </div>
                
                <div className="panel-body">
                <form onSubmit={this.onSubmitCreate}>
                    
                    <div className="row">
                    
                        <div className="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <label htmlFor="urlkey">URL suffix</label>
                            <input type="text" id="urlkey" className="form-control" value={this.state.urlkey} onChange={this.onChangeURLKey} />
                        </div>
                        
                        <div className="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            
                            <label htmlFor="password">Password</label>                   
                            
                            <div className="input-group">
                                <input type="password" id="password" className="form-control" value={this.state.password} onChange={this.onChangePassword} />
                                <span className="input-group-btn">
                                    <button type="submit" className="btn btn-primary">
                                        <span className="glyphicon glyphicon-log-in"></span> Create
                                    </button>
                                </span>
                            </div>
                            
                        </div>
                    </div>
                    <div className="row">
                        
                        <div className="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <span className="glyphicon glyphicon-globe"></span> {_ROUTES["clientroot"]() + "/#view/" + this.state.urlkey}
                        </div>
                        
                    </div>
                    
                </form>
                </div>
            </div>
        );
    }
});

var Instructions = React.createClass({
    render: function(){
        return (
            <div className="jumbotron">
                <h1>Instructions</h1>
                <p><strong>Step 1:</strong> Choose a URL and password to create a new list of feeds</p>
                <p><strong>Step 2:</strong> Add some feeds to the list (the password is required to edit)</p>
                <p><strong>Step 3:</strong> View the list at any time via your URL (no password required)</p>
                
                <small>Built with Laravel, React, Redux.  Source: <a href="https://bitbucket.org/tdmsrc/lvfeeds">https://bitbucket.org/tdmsrc/lvfeeds</a></small>
            </div>
        );
    }
});


var mapStateToProps = (state) => ({
    pendingAJAX:    state.ajax.pending,
});

export default connect(mapStateToProps)(HomePage);
