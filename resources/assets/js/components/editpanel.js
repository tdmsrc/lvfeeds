
import React from "react";
import { connect } from "react-redux";

import { FeedsActions } from "../store/feeds";
import { default as LoginForm } from "./loginform";


var EditPanel = React.createClass({

    addFeed: function(url, onSuccess, onFail){
        this.props.dispatch(FeedsActions.remoteAdd(this.props.urlkey, url, onSuccess, onFail));
    },
    
    removeFeed: function(id, onSuccess, onFail){
        this.props.dispatch(FeedsActions.remoteRemove(this.props.urlkey, id, onSuccess, onFail));
    },
    
    render: function(){
        return (
            <div>
                <LoginForm urlkey={this.props.urlkey} />
                
                <CurrentFeedsPanel removeFeed={this.removeFeed} feedsPreview={this.props.feedsPreview} />
                <AddFeedPanel addFeed={this.addFeed} />
                <SuggestedFeedsPanel addFeed={this.addFeed} feedsSuggested={this.props.feedsSuggested} />
            </div>
        );
    }
});

var CurrentFeedsPanel = React.createClass({
    
    getInitialState: function(){
        return { 
            formError: "",
            formErrorExists: false
        };
    },
    
    renderFormError: function(){
        if(!this.state.formErrorExists){ return false; }
        
        return (
            <span className="label label-danger pull-right" onClick={this.onClickDismissFormError}>
                <span className="glyphicon glyphicon-exclamation-sign"></span>
                &nbsp;
                {this.state.formError}
            </span>
        );
    },
    onClickDismissFormError: function(){
        this.setState({ formError: "", formErrorExists: false });        
    },
    onSuccess: function(dispatch, data){
        this.setState({ formError: "", formErrorExists: false }); 
    },
    onFail: function(dispatch, errmsg){
        this.setState({ formError: errmsg, formErrorExists: true });
    },
    
    renderCurrentEmpty: function(){
        return (
            <div className="alert alert-warning">
                No feeds have been added yet.
            </div>
        );
    },
    
    render: function(){
        if(this.props.feedsPreview.length == 0){
            return this.renderCurrentEmpty();
        }
        
        var current = this.props.feedsPreview.map((preview) => (
            <CurrentFeed 
                key={preview.id} 
                status={preview.status} 
                feedData={preview.feedData} 
                count={preview.count} 
                remove={() => this.props.removeFeed(preview.id, this.onSuccess, this.onFail)} 
            />
        ));
        
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    Current Feeds
                    {this.renderFormError()}
                </div>
                
                <ul className="list-group">{current}</ul>
            </div>
        );
    }
});

var AddFeedPanel = React.createClass({
    
    getInitialState: function(){
        return { 
            formError: "",
            formErrorExists: false,
            feedurl: "" 
        };
    },
    
    renderFormError: function(){
        if(!this.state.formErrorExists){ return false; }
        
        return (
            <span className="label label-danger pull-right" onClick={this.onClickDismissFormError}>
                <span className="glyphicon glyphicon-exclamation-sign"></span>
                &nbsp;
                {this.state.formError}
            </span>
        );
    },
    onClickDismissFormError: function(){
        this.setState({ formError: "", formErrorExists: false });        
    },
    onSuccess: function(dispatch, data){
        this.setState({ formError: "", formErrorExists: false }); 
    },
    onFail: function(dispatch, errmsg){
        this.setState({ formError: errmsg, formErrorExists: true });
    },
    
    onChangeFeedURL: function(e){
        this.setState({ feedurl: e.target.value });
    },
    
    onSubmitAdd: function(e){
        this.props.addFeed(this.state.feedurl, this.onSuccess, this.onFail);
        this.setState({ feedurl: "" });
        
        e.preventDefault();
    },
    
    render: function(){
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    Add Custom Feed
                    {this.renderFormError()}
                </div>
                
                <div className="panel-body">
                <form onSubmit={this.onSubmitAdd}>
                
                    <div className="input-group">
                        <span className="input-group-btn">
                            <button type="submit" className="btn btn-primary">
                                <span className="glyphicon glyphicon-plus"></span>
                            </button>
                        </span>
                        <input type="text" className="form-control" placeholder="Enter the feed's URL" value={this.state.feedurl} onChange={this.onChangeFeedURL} />
                    </div>
                    
                </form>
                </div>
            </div>
        );
    }
});

var SuggestedFeedsPanel = React.createClass({
    
    getInitialState: function(){
        return { 
            formError: "",
            formErrorExists: false
        };
    },
    
    renderFormError: function(){
        if(!this.state.formErrorExists){ return false; }
        
        return (
            <span className="label label-danger pull-right" onClick={this.onClickDismissFormError}>
                <span className="glyphicon glyphicon-exclamation-sign"></span>
                &nbsp;
                {this.state.formError}
            </span>
        );
    },
    onClickDismissFormError: function(){
        this.setState({ formError: "", formErrorExists: false });        
    },
    onSuccess: function(dispatch, data){
        this.setState({ formError: "", formErrorExists: false });
    },
    onFail: function(dispatch, errmsg){
        this.setState({ formError: errmsg, formErrorExists: true });
    },
    
    render: function(){
        var suggested = this.props.feedsSuggested.map((url) => (
            <SuggestedFeed 
                key={url} 
                url={url} 
                add={() => this.props.addFeed(url, this.onSuccess, this.onFail)}
            />
        ));
        
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    Add Suggested Feeds
                    {this.renderFormError()}
                </div>
                
                <ul className="list-group">{suggested}</ul>
            </div>
        );
    }
});

var CurrentFeed = React.createClass({

    renderStatusIcon: function(){
        switch(this.props.status){
        case "done":
            return (<span className="glyphicon glyphicon-ok"></span>);
        case "pending":
            return (<span className="glyphicon glyphicon-spin glyphicon-refresh"></span>);
        case "broken":
            return (<span className="glyphicon glyphicon-remove-circle"></span>);
        default:
            return (<span className="glyphicon glyphicon-question-sign"></span>);
        }
    },
    
    render: function(){
        return (
            <li className="list-group-item">
                <button type="button" className="btn btn-primary" onClick={this.props.remove}>
                    <span className="glyphicon glyphicon-minus"></span>
                </button>
                &nbsp;
                <span>
                    <img className="feed-icon" src={this.props.feedData.icon} />{this.props.feedData.title}
                </span>
                
                <span className="badge">
                    {this.props.count}&nbsp;{this.renderStatusIcon()}
                </span>
            </li>
        );
    }
});

var SuggestedFeed = React.createClass({

    onSubmitAdd: function(e){
        this.props.add();
        e.preventDefault();
    },
    
    render: function(){
        return (
            <li className="list-group-item">
            <form onSubmit={this.onSubmitAdd}>
            
                <div className="input-group">
                    <span className="input-group-btn">
                        <button type="submit" className="btn btn-primary">
                            <span className="glyphicon glyphicon-plus"></span>
                        </button>
                    </span>
                    <input type="text" className="form-control" readOnly placeholder="Enter the feed's URL" value={this.props.url}/>
                </div>
                
            </form>
            </li>
        );
    }
});


var derivedFeedsPreview = function(feeds){ 
    var feedsPreview = [];
    
    for(var id in feeds){
        var feed = feeds[id];
        feedsPreview.push({
            id:         id,
            status:     feed.status,
            feedData:   feed.data.feedData,
            count:      feed.data.items.length
        });
    }
    
    return feedsPreview;
};

//additional prop: urlkey
var mapStateToProps = (state) => ({
    feedsSuggested: state.feeds.suggested,
    feedsPreview:   derivedFeedsPreview(state.feeds.feeds)
});

export default connect(mapStateToProps)(EditPanel);

