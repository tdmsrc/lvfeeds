
import React from "react";
import { connect } from "react-redux";
import { createSelector } from "reselect";

import { default as DateTimeHelper } from "../lib/datetime";


var ViewFeeds = React.createClass({
    renderGroupNodes: function(){
    
        var groupNodes = this.props.merged.map((group) => (
            <FeedItemGroup key={group.key} title={group.groupTitle} sortedItems={group.sortedItems} />
        ));
        
        if(groupNodes.length == 0){
            return (
                <div className="alert alert-warning">
                    <strong>No items!</strong>  You may need to add a feed.
                </div>
            );
        }
        
        else{ return groupNodes; }
    },
    
    render: function(){
        return (
            <div>
                {this.renderGroupNodes()}
            </div>
        );
    }
});

var FeedItemGroup = React.createClass({
    render: function(){
        
        var feedNodes = this.props.sortedItems.map((sortedItem) => (
            <FeedItem key={sortedItem.key} sortedItem={sortedItem} />
        ));
        
        return (
            <div>
                <FeedItemGroupTitle title={this.props.title} />
                {feedNodes}
            </div>
        );
    }
});

var FeedItemGroupTitle = React.createClass({
    render: function(){
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    {this.props.title}
                </div>
            </div>
        );
    }
});

var FeedItem = React.createClass({
    
    render: function(){
        var itemData = this.props.sortedItem.itemData;
        var feedData = this.props.sortedItem.feedData;
        
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <img className="feed-icon" src={feedData.icon} /><a href={itemData.link}>{itemData.title}</a>
                </div>
                 
                <div className="panel-body">
                    <p>
                        {itemData.description}
                    </p>
                    
                    <span className="feed-item-footer">
                        <a href={feedData.link}>{feedData.title}</a> - <a href={feedData.url}>Feed</a>
                    </span>
                    <span className="feed-item-footer pull-right">
                        <span className="glyphicon glyphicon-time"></span> {DateTimeHelper.prettyTimestamp(itemData.timestamp)}
                    </span>
                </div>
            </div>
        );
    }
});


var derivedMerged = function(feeds){
    //first assemble sorted array of sortedItems
    var sorted = [];
    
    for(var id in feeds){
        var feed = feeds[id];
        
        for(var i=0; i<feed.data.items.length; i++){
            var item = feed.data.items[i];
            
            sorted.push({
                key:        item.title,
                feedData:   feed.data.feedData, 
                itemData:   item
            });
        }
    }
    
    sorted = sorted.sort((a,b) => {
        return b.itemData.timestamp - a.itemData.timestamp;
    });
    
    //now group into hourly bins
    var groups = [];
    
    var newGroupItems = function(hourTS){
        var group = {
            key:            hourTS,
            groupTitle:     "Before " + DateTimeHelper.prettyTimestamp(hourTS),
            sortedItems:    []
        };
        groups.push(group);
        return group.sortedItems;
    };
    
    var lastHourTS = -1;
    var currentGroupItems;
    
    for(var i=0; i<sorted.length; i++){
        var sortedItem = sorted[i];
    
        //round up to next hour
        var ts = sortedItem.itemData.timestamp;
        var hourTS = DateTimeHelper.nextHourTimestamp(ts);
        
        if(hourTS != lastHourTS){
            currentGroupItems = newGroupItems(hourTS);
            lastHourTS = hourTS;
        }
        
        //put in new group
        currentGroupItems.push(sortedItem);
    }
    
    return groups;
};

var derivedMergedSelector = createSelector(
    (state) => state.feeds.feeds,
    (feeds) => derivedMerged(feeds)
);

var mapStateToProps = (state) => ({
    merged: derivedMergedSelector(state)
    //merged: derivedMerged(state.feeds.feeds)
});

export default connect(mapStateToProps)(ViewFeeds);

