<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post("/auth/login", "CustomAuthController@login")->name("login");
Route::post("/auth/logout", "CustomAuthController@logout")->name("logout");

Route::post("/api/suggested", "APIController@getSuggested")->name("suggested");
Route::post("/api/get", "APIController@getFeeds")->name("get");
Route::post("/api/add", "APIController@addFeed")->name("add");
Route::post("/api/remove", "APIController@removeFeed")->name("remove");
Route::post("/api/adduser", "APIController@addUser")->name("adduser");

Route::get("/feed", "FeedController@getFeed")->name("feedapi");

Route::get("/", "ClientController@show")->name("show");
