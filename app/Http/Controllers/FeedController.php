<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Services\FeedService;


class FeedController extends Controller
{
    protected $svcFeeds;
        
    //TODO fix this
    //---------------------------------
    private function makeStandardResponse($success, $data){
        return [
            "success" => ($success ? TRUE : FALSE),
            "data" => $data
        ];
    }
    //---------------------------------
    
    
    public function __construct(FeedService $svcFeeds){
        $this->svcFeeds = $svcFeeds;
    }
    
    public function getFeed(Request $request){
    
        $url = $request->input("url");
        
        try{
            $feedData = $this->svcFeeds->get($url);
            return $this->makeStandardResponse(TRUE, $feedData);
        }catch(\Exception $e){
            return $this->makeStandardResponse(FALSE, $e->getMessage());
        }
    }
}
