<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;

use App\Services\UserService;


class APIController extends Controller
{
    protected $svcUser;
    
    //TODO fix this; dupe in CustomAuthController
    //---------------------------------
    private function makeStandardResponse($success, $data){
        return [
            "success" => ($success ? TRUE : FALSE),
            "data" => $data
        ];
    }
    //---------------------------------
    
    //TODO authorization
    //---------------------------------
    public function isAuthorized($urlkey){
        $authorized = FALSE;
        
        if(Auth::check()){
            if(strcasecmp(Auth::user()->urlkey, $urlkey) == 0){
                $authorized = TRUE;
            }
        }
        
        return $authorized;
    }
    //---------------------------------
    
    public function __construct(UserService $svcUser){
        $this->svcUser = $svcUser;
    }
    
    public function getSuggested(){
        $feedurls = $this->svcUser->getSuggestedFeedURLs();
        return $this->makeStandardResponse(TRUE, $feedurls);
    }
    
    public function addUser(Request $request){
        $urlkey = $request->input("urlkey");
        $password = $request->input("password");
        
        try{
            $this->svcUser->createUser($urlkey, $password);
            return $this->makeStandardResponse(TRUE, "");
        }catch(\Exception $e){
            return $this->makeStandardResponse(FALSE, $e->getMessage());            
        }
    }
    
    public function getFeeds(Request $request){
        $urlkey = $request->input("urlkey");
        
        try{
            $ret = $this->svcUser->getFeeds($urlkey);
            return $this->makeStandardResponse(TRUE, $ret);
        }catch(\Exception $e){
            return $this->makeStandardResponse(FALSE, $e->getMessage());
        }
    }
    
    public function addFeed(Request $request){
        $urlkey = $request->input("urlkey");
        $feedurl = $request->input("feedurl");
        
        if(!$this->isAuthorized($urlkey)){
            return $this->makeStandardResponse(FALSE, "Permission denied");            
        }
        
        try{
            $ret = $this->svcUser->addFeed($urlkey, $feedurl);
            return $this->makeStandardResponse(TRUE, $ret);
        }catch(\Exception $e){
            return $this->makeStandardResponse(FALSE, $e->getMessage());
        }
    }
    
    public function removeFeed(Request $request){
        $urlkey = $request->input("urlkey");
        $feedid = $request->input("feedid");
        
        if(!$this->isAuthorized($urlkey)){
            return $this->makeStandardResponse(FALSE, "Permission denied");            
        }
        
        try{
            $this->svcUser->removeFeed($urlkey, $feedid);
            return $this->makeStandardResponse(TRUE, "");
        }catch(\Exception $e){
            return $this->makeStandardResponse(FALSE, $e->getMessage());
        }
    }
}
