<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;


class CustomAuthController extends Controller
{
    //TODO fix this; dupe in APIController
    //---------------------------------
    private function makeStandardResponse($success, $data){
        return [
            "success" => ($success ? TRUE : FALSE),
            "data" => $data
        ];
    }
    //---------------------------------
    
    //NOTE: logs out on failure
    public function login(Request $request){
        $urlkey = $request->input("urlkey");
        $password = $request->input("password");
        
        $success = Auth::attempt([
            "urlkey"=>$urlkey, 
            "password"=>$password
        ]);
        
        if($success){
            return $this->makeStandardResponse(TRUE, "");
        }else{
            Auth::logout();
            return $this->makeStandardResponse(FALSE, "Login failed");
        }
    }
    
    public function logout(){
        Auth::logout();
        return $this->makeStandardResponse(TRUE, "");
    }
}
