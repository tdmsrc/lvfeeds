<?php

namespace App\Services;


class FeedService{
    
    public function getData($url){
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        
        $data = curl_exec($ch);
        if($data == FALSE){
            throw new \Exception("Unable to get feed: " . curl_error($ch));
        }
        
        curl_close($ch);
        return $data;
    }
    
    //only searches for direct children, and also matches tag exactly
    //e.g. getElementsByTagName("link") will also find <atom:link>
    function getFirstChildValue($node, $tagName){
    
        foreach($node->childNodes as $childNode){
            //skip e.g. DomText
            if(!is_a($childNode, "DomElement")){ continue; }
            
            if($childNode->tagName === $tagName){
                return $childNode->nodeValue;
            }
        }
        
        return "";
    }
    
    //encode a JSON array of items
    //- POST in: "url" and "tzOffset"
    //- JSON out: "items" = array of items with "HTML", "timestamp"; "feedData"
    public function get($feedURL){
        
        //get target page XML
        $feedXML = $this->getData($feedURL);
        
        //create XML document
        $feedDOM = new \DOMDocument();
        $feedDOM->loadXML($feedXML);
        
        //prepare feed data
        $channelNode = $feedDOM->getElementsByTagName('channel')->item(0);
        $feedData = array(
            'url'         => $feedURL, 
            'link'        => $this->getFirstChildValue($channelNode, 'link'), 
            'title'       => strip_tags($this->getFirstChildValue($channelNode, 'title')),
            'description' => strip_tags($this->getFirstChildValue($channelNode, 'description')),
            'icon'        => ""
        );
        
        //feed icon
        $parsedURL = parse_url($feedData['link']);
        
        //TODO embedding http images causes warnings when site has forced https
        //$feedData['icon'] = $parsedURL['scheme'] . "://" . $parsedURL['host'] . "/favicon.ico";
        $feedData['icon'] = "https://images.weserv.nl/?url=" . $parsedURL['host'] . "/favicon.ico";
        
        //find feed items
        $items = array();
        
        foreach($feedDOM->getElementsByTagName('item') as $itemNode){
            //prepare item data
            $itemData = array(
                'link'        => $this->getFirstChildValue($itemNode, 'link'), 
                'title'       => strip_tags($this->getFirstChildValue($itemNode, 'title')),
                'description' => strip_tags($this->getFirstChildValue($itemNode, 'description')),
                'timestamp'   => 0
            );
            
            //item timestamp
            $pubDate = $this->getFirstChildValue($itemNode, 'pubDate');
            if(($itemData['timestamp'] = strtotime($pubDate)) === false){
              $itemData['timestamp'] = 0;
            }
            
            $items[] = $itemData;
        }
        
        //encode and return results
        return array('feedData' => $feedData, 'items' => $items);
    }
}
