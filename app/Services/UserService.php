<?php

namespace App\Services;

use DB;


class UserService{
    
    //returns user row or throws exception if user doesn't exist
    public function findUser($urlkey){
    
        $user = DB::table("users")->where("urlkey", $urlkey)->first();
        if($user === null){
            throw new \Exception("No such user: " . $urlkey);
        }
        
        return $user;
    }
    
    //return array of url strings
    public function getSuggestedFeedURLs(){
        $feeds = DB::table("suggested")->get();
        
        $data = [];
        foreach($feeds as $feed){
            $data[] = $feed->url;
        }
        return $data;
    }
    
    //create a user
    public function createUser($urlkey, $password){
        //validate
        if(strlen($urlkey) < 1){ 
            throw new \Exception("URL key must be at least 1 character");
        }
        if(strlen($password) < 1){
            throw new \Exception("Password must be at least 1 character");
        }
        
        $user = DB::table("users")->where("urlkey", $urlkey)->first();
        if($user !== null){
            throw new \Exception("User already exists");
        }
        
        //create
        $id = DB::table("users")->insertGetId([
            "urlkey" => $urlkey,
            "password" => password_hash($password, PASSWORD_DEFAULT)
        ]);
    }
    
    //return array of {feedid, url}
    public function getFeeds($urlkey){
        $user = $this->findUser($urlkey);
        $feeds = DB::table("feeds")->where("userid", $user->id)->get();
        
        $data = [];
        foreach($feeds as $feed){
            $data[] = [
                "id" => $feed->id,
                "url" => $feed->url
            ];
        }
        return $data;
    }
    
    //add feed for a user
    public function addFeed($urlkey, $feedurl){
        $user = $this->findUser($urlkey);
        
        $id = DB::table("feeds")->insertGetId([
            "url" => $feedurl,
            "userid" => $user->id
        ]);
        
        return [
            "id" => $id,
            "url" => $feedurl
        ];
    }
    
    //remove feed from a user
    public function removeFeed($urlkey, $feedid){
        $user = $this->findUser($urlkey);
        
        $count = DB::table("feeds")->where("id", $feedid)->where("userid", $user->id)->delete();
        if($count == 0){ 
            throw new \Exception("No such feed");
        }
        
        return $count;
    }
    
}
