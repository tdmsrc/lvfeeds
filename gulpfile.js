var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix){  
    if(elixir.config.production){
        process.env.NODE_ENV = 'production';
    }
    
    mix.less("main.less");
    mix.browserify("show.js", "public/js/show.js");
});
